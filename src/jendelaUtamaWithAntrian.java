
import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import com.github.sarxos.webcam.WebcamResolution;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.swing.ImageIcon;
import javax.swing.Timer;
import utility.ImagePanel;
import utility.getConnection;
import utility.getWaktu;
import utility.penangananKomponen;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import java.util.Hashtable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author iweks
 */
public class jendelaUtamaWithAntrian extends javax.swing.JFrame implements Runnable, ThreadFactory {

    int bersih = 11;
    getConnection u;
    Connection kon;
    penangananKomponen kom;
    String id_siswa = "0";
    String email_siswa = "0";
    String email_ortu = "0";
    String foto = "";
    String filePath = "D:/Tom.png";
    String nofilePath = "D:/Tom.png";
    private static final int BUFFER_SIZE = 4096;
    private static final long serialVersionUID = 6441489157408381878L;

    private Executor executor = Executors.newSingleThreadExecutor(this);
    private Webcam webcam = null;
    private WebcamPanel panel = null;

    /**
     * Creates new form jendelaUtama
     */
    public jendelaUtamaWithAntrian() {
        super();
        filePath = System.getProperty("user.dir") + "\\image\\foto.png";
        nofilePath = System.getProperty("user.dir") + "\\image\\no_foto.png";
        u = new getConnection();
        kon = u.getConnection(u.jdbc, u.url, u.user, u.pass);
        kom = new penangananKomponen();

        if (kon == null) {
            setVisible(false);
            new pengaturan();
        } else {
            initComponents();
            b_masuk.setVisible(false);
            //      b_keluar.setVisible(false);

            final DateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy -- HH:mm:ss");
            ActionListener taskPerformer = new ActionListener() {
                public void actionPerformed(ActionEvent evt) {

                    java.util.Date date = new java.util.Date();
                    String datestring = dateFormat.format(date);
                    jam.setText(datestring);
                    bersih++;
                    if (bersih == 10) {
                        awalAbsen();
                    }
                }
            };
            // Timer
            new Timer(1000, taskPerformer).start();

            java.awt.Dimension screen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
            setSize(screen);
            setLocation(0, 0);
            awalAbsen();
            Dimension size = WebcamResolution.QVGA.getSize();

//            webcam = Webcam.getWebcams().get(0);
//            webcam.setViewSize(size);
//
//            panel = new WebcamPanel(webcam);
//            panel.setPreferredSize(size);
//            camera.add(panel);
//            setVisible(true);
            scanNIS.requestFocus();
  //          executor.execute(this);
        }
    }

    @Override
    public void run() {

        do {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Result result = null;
            BufferedImage image = null;

            if (webcam.isOpen()) {

                if ((image = webcam.getImage()) == null) {
                    continue;
                }

                LuminanceSource source = new BufferedImageLuminanceSource(image);
                BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

                try {
                    result = new MultiFormatReader().decode(bitmap);
                } catch (NotFoundException e) {
                    //       System.out.println(e.getMessage());
// fall thru, it means there is no QR code in image
                }
            }

            if (result != null) {
                textNIS.setText(result.getText());
                cekNIS();
            }

        } while (true);
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(r, "example-runner");
        t.setDaemon(true);
        return t;
    }

    private synchronized void loadImage(String id) {
        //String id = tmb_id.getText();
        gambar.setText("");
        gambar.setIcon(new ImageIcon(new ImageIcon(nofilePath).getImage().getScaledInstance(gambar.getWidth(), gambar.getHeight(), java.awt.Image.SCALE_SMOOTH)));
        //  gambar.setText("FOTO");
        String sql = "SELECT foto FROM musik_pihak_foto WHERE id_pihak = " + id;
        try {
            PreparedStatement stat = kon.prepareStatement(sql);
            ResultSet rset = stat.executeQuery();
            if (rset.next()) {
                Blob blob = rset.getBlob(1);
                InputStream inputStream = blob.getBinaryStream();
                OutputStream outputStream = new FileOutputStream(filePath);

                int bytesRead = -1;
                byte[] buffer = new byte[BUFFER_SIZE];
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                inputStream.close();
                outputStream.close();
                gambar.setText("");
                gambar.setIcon(new ImageIcon(new ImageIcon(filePath).getImage().getScaledInstance(gambar.getWidth(), gambar.getHeight(), java.awt.Image.SCALE_SMOOTH)));

            }
        } catch (SQLException ex) {
            // Logger.getLogger(Foto.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            //  Logger.getLogger(Foto.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            //   Logger.getLogger(Foto.class.getName()).log(Level.SEVERE, null, ex);
        }

        gambar.setVisible(true);
    }

//    private void ngomong(String data) {
//        System.setProperty("mbrola.base",
//                "C:\\mbrola");
//        VoiceManager vm = VoiceManager.getInstance();
//
//        Voice[] voices = vm.getVoices();
//        //   Voice voicex = null;
//        //    for (int i = 0; i < voices.length; i++) {
//        //        System.out.println(voices[i].getName());
//        //    }
//
//        voice = vm.getVoice("mbrola_us1");
//        //    voice.setPitch(100.0f);
//        //      System.out.println(voice.getPitch());
//        voice.allocate();
//
//        voice.speak(data);
//    }
    private void cekNIS() {

        tombol.setVisible(false);
        scanNIS.setVisible(false);
        String nis = textNIS.getText().substring(0, 20);
        String sql = "SELECT nama, panggilan, IFNULL(no_hp_ortu, '0') as nohp, id, email, email_ortu FROM musik_master_siswa WHERE nis = '" + nis + "'";

        if (nis.equalsIgnoreCase("qazwsxedcrfv12345678")) {
            System.exit(1);
        } else {
            Object[] data = kom.setDataEdit(kon, sql);
            try {
                textNama.setText(data[0].toString());
                textPanggilan.setText(data[1].toString());
                textNoHP.setText(data[2].toString());
                id_siswa = data[3].toString();
                email_siswa = data[4].toString();
                email_ortu = data[5].toString();
                b_masuk.setVisible(true);
//                b_keluar.setVisible(true);
//                b_keluar1.setVisible(true);
                loadImage(id_siswa);
//                camera.setVisible(false);
                simpanMasuk();
                bersih = 8;
            } catch (Exception e) {
//                ngomong("DATA... NOT FOUND");
//                ngomong("PLEASE... TRY AGAIN");
                awalAbsen();
                bersih = 8;
            }
        }
    }

    private void awalAbsen() {
        textNIS.setText("");
        textNama.setText("");
        textPanggilan.setText("");
        textNoHP.setText("");
        tombol.setVisible(true);
        b_masuk.setVisible(false);
//        b_keluar.setVisible(false);
//        b_keluar1.setVisible(false);
        scanNIS.setText("");
        scanNIS.setVisible(true);
        scanNIS.requestFocus();
        gambar.setVisible(false);
//        camera.setVisible(true);
    }

    private boolean simpanDataAbsensi(String status) {

        String sql = "INSERT INTO musik_siswa_presensi(id_siswa, absen, absen_status) VALUES('" + id_siswa + "',now(),'" + status + "')";
        return kom.setSQL(kon, sql);

    }

    private void inputNIS(String ak) {
        String n = textNIS.getText();
        textNIS.setText(n + ak);
        scanNIS.requestFocus();
    }

    private void kirimPesan(String absen, String methode, String[][] atribut, String tujuan, String judul) {
        String sql = "SELECT template FROM musik_template_pesan WHERE nama = '" + absen + "' AND methode = '" + methode + "'";
        String pesan = kom.getStringSQL(kon, sql);

        int xx = atribut.length;
        for (int y = 0; y < xx; y++) {
            String a = atribut[y][0].toString();
            String b = atribut[y][1].toString();
            pesan = pesan.replaceAll("<" + a + ">", b);
        }

        kom.kirimPesan(kon, tujuan, Integer.parseInt(id_siswa), pesan, methode, judul);
    }

    private void simpanMasuk() {
        if (simpanDataAbsensi("MASUK")) {
            String[][] dataSiswa = {
                {"nama_siswa", textNama.getText()},
                {"waktu", new getWaktu().getWaktu()}};
            kirimPesan("ABSEN_MASUK", "SMS", dataSiswa, textNoHP.getText(), "-");
            kirimPesan("ABSEN_MASUK_EMAIL", "EMAIL", dataSiswa, email_siswa, "PRESENSI MASUK");
            kirimPesan("ABSEN_MASUK_EMAIL", "EMAIL", dataSiswa, email_ortu, "PRESENSI MASUK");
            //        b_masuk.setVisible(false);
//            b_keluar.setVisible(false);
            //           b_keluar1.setVisible(false);
            //        ngomong("YOUR NAME IS " + textPanggilan.getText() + "!");
//            ngomong("WELCOME AND GOOD LUCK");
            //        bersih = 7;
        }
    }

    private void simpanAntrian(String kep) {
        int nom = kom.getIntSQL(kon, "SELECT count(1) + 1 as nom FROM musik_antrian_cso WHERE tanggal = cast(now() as date)");
        String[][] data2 = {
            {"tanggal", "now()"},
            {"urut", String.valueOf(nom)},
            {"keperluan", kep},
            {"create_date", "now()"}
        };
        int ok = kom.simpanDataReturn(kon, "musik_antrian_cso", data2, false);
        //JOptionPane.showMessageDialog(this, ok);
        
        ngeprint(String.valueOf(ok));
        scanNIS.requestFocus();
    }

    private void ngeprint(String id) {
        try {
            String sql = "SELECT pesan FROM musik_antrian_pesan WHERE dari_tanggal <= cast(now() as date) AND sampai_tanggal >= cast(now() as date)";
            String pesan = kom.getStringSQL(kon, sql);
            String folder = System.getProperty("user.dir") + "\\Laporan\\antrian.jasper";
            Hashtable hparam = new Hashtable(1);

            hparam.put("id", id);
            hparam.put("pesan", pesan);

            JasperReport report = (JasperReport) JRLoader.loadObject(folder);
            JasperPrint JPrint = JasperFillManager.fillReport(report, hparam, kon);

            PrintService printService = PrintServiceLookup.lookupDefaultPrintService();
            if (printService == null) {

                //  throw new PrinterException("No Printer Attached / Shared to the server");
            } else {

                JRExporter exporter = new JRPrintServiceExporter();
                exporter.setParameter(JRExporterParameter.JASPER_PRINT, JPrint);
                exporter.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE_ATTRIBUTE_SET, printService.getAttributes());
                exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PAGE_DIALOG, Boolean.FALSE);
                exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PRINT_DIALOG, Boolean.FALSE);
                exporter.exportReport();

            }

        } catch (JRException ex) {
            Logger.getLogger(jendelaUtamaWithAntrian.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void simpanKeluar() {
        if (simpanDataAbsensi("PULANG")) {
            String[][] dataSiswa = {
                {"nama_siswa", textNama.getText()},
                {"waktu", new getWaktu().getWaktu()}};
            kirimPesan("ABSEN_PULANG", "SMS", dataSiswa, textNoHP.getText(), "-");
            kirimPesan("ABSEN_PULANG_EMAIL", "EMAIL", dataSiswa, email_siswa, "PRESENSI PULANG");
            kirimPesan("ABSEN_PULANG_EMAIL", "EMAIL", dataSiswa, email_ortu, "PRESENSI PULANG");
            b_masuk.setVisible(false);
//            b_keluar.setVisible(false);
            //           b_keluar1.setVisible(false);
            //        ngomong("YOUR NAME IS " + textPanggilan.getText() + "!");
//            ngomong("THANK YOU AND SEE YOU LATTER");

            bersih = 7;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        xxxyy = new javax.swing.JPanel();
        data = new javax.swing.JPanel();
        textNIS = new javax.swing.JTextField();
        b_masuk = new javax.swing.JButton();
        textNoHP = new javax.swing.JTextField();
        textNama = new javax.swing.JTextField();
        textPanggilan = new javax.swing.JTextField();
        gambar = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        scanNIS = new javax.swing.JTextField();
        jPanel7 = new javax.swing.JPanel();
        jam = new javax.swing.JLabel();
        tombol = new javax.swing.JPanel();
        btnPembayarn = new javax.swing.JButton();
        btnKonsultasi = new javax.swing.JButton();
        btnTrial = new javax.swing.JButton();
        btnPendaftaran = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setExtendedState(this.MAXIMIZED_BOTH);
        setUndecorated(true);
        setResizable(false);

        xxxyy = new ImagePanel();
        xxxyy.setLayout(new java.awt.BorderLayout());

        data.setOpaque(false);
        data.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        textNIS.setEditable(false);
        textNIS.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        textNIS.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        textNIS.setBorder(null);
        textNIS.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        textNIS.setOpaque(false);
        textNIS.setRequestFocusEnabled(false);
        textNIS.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                textNISCaretUpdate(evt);
            }
        });
        data.add(textNIS, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 860, -1));

        b_masuk.setBackground(new java.awt.Color(0, 153, 0));
        b_masuk.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        b_masuk.setText("MASUK");
        b_masuk.setPreferredSize(new java.awt.Dimension(150, 150));
        b_masuk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_masukActionPerformed(evt);
            }
        });
        data.add(b_masuk, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 30, -1, -1));

        textNoHP.setEditable(false);
        textNoHP.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        textNoHP.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        textNoHP.setBorder(null);
        textNoHP.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        textNoHP.setOpaque(false);
        textNoHP.setRequestFocusEnabled(false);
        textNoHP.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                textNoHPCaretUpdate(evt);
            }
        });
        data.add(textNoHP, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 290, 860, -1));

        textNama.setEditable(false);
        textNama.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        textNama.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        textNama.setBorder(null);
        textNama.setOpaque(false);
        textNama.setRequestFocusEnabled(false);
        textNama.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                textNamaCaretUpdate(evt);
            }
        });
        data.add(textNama, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 860, -1));

        textPanggilan.setEditable(false);
        textPanggilan.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        textPanggilan.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        textPanggilan.setBorder(null);
        textPanggilan.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        textPanggilan.setOpaque(false);
        textPanggilan.setRequestFocusEnabled(false);
        textPanggilan.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                textPanggilanCaretUpdate(evt);
            }
        });
        data.add(textPanggilan, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 190, 860, -1));

        gambar.setBackground(new java.awt.Color(255, 255, 255));
        gambar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        gambar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        gambar.setText("FOTO");
        gambar.setToolTipText("");
        gambar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        gambar.setOpaque(true);
        gambar.setPreferredSize(new java.awt.Dimension(240, 240));
        data.add(gambar, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 350, -1, -1));

        xxxyy.add(data, java.awt.BorderLayout.CENTER);

        jPanel2.setOpaque(false);
        jPanel2.setLayout(new java.awt.GridLayout(1, 0));

        jPanel1.setOpaque(false);
        jPanel1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 30, 30));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/logo.png"))); // NOI18N
        jPanel1.add(jLabel1);

        jPanel2.add(jPanel1);

        jPanel6.setOpaque(false);
        jPanel6.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT, 20, 32));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel2.setText("SMI SEMARANG");
        jPanel6.add(jLabel2);

        jPanel2.add(jPanel6);

        xxxyy.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        jPanel3.setOpaque(false);
        jPanel3.setLayout(new java.awt.GridLayout(1, 0));

        jPanel4.setOpaque(false);
        jPanel4.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        scanNIS.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        scanNIS.setPreferredSize(new java.awt.Dimension(500, 50));
        scanNIS.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                scanNISCaretUpdate(evt);
            }
        });
        jPanel4.add(scanNIS);

        jPanel3.add(jPanel4);

        jPanel7.setOpaque(false);
        jPanel7.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT, 10, 10));

        jam.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jPanel7.add(jam);

        jPanel3.add(jPanel7);

        xxxyy.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        tombol.setOpaque(false);
        tombol.setPreferredSize(new java.awt.Dimension(400, 100));
        tombol.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT, 10, 10));

        btnPembayarn.setBackground(new java.awt.Color(153, 153, 255));
        btnPembayarn.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnPembayarn.setText("Pembayaran");
        btnPembayarn.setPreferredSize(new java.awt.Dimension(300, 100));
        btnPembayarn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPembayarnActionPerformed(evt);
            }
        });
        tombol.add(btnPembayarn);

        btnKonsultasi.setBackground(new java.awt.Color(153, 255, 153));
        btnKonsultasi.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnKonsultasi.setText("Konsultasi");
        btnKonsultasi.setPreferredSize(new java.awt.Dimension(300, 100));
        btnKonsultasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKonsultasiActionPerformed(evt);
            }
        });
        tombol.add(btnKonsultasi);

        btnTrial.setBackground(new java.awt.Color(255, 204, 204));
        btnTrial.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnTrial.setText("Trial");
        btnTrial.setPreferredSize(new java.awt.Dimension(300, 100));
        btnTrial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTrialActionPerformed(evt);
            }
        });
        tombol.add(btnTrial);

        btnPendaftaran.setBackground(new java.awt.Color(255, 255, 102));
        btnPendaftaran.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnPendaftaran.setText("Pendaftaran");
        btnPendaftaran.setPreferredSize(new java.awt.Dimension(300, 100));
        btnPendaftaran.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPendaftaranActionPerformed(evt);
            }
        });
        tombol.add(btnPendaftaran);

        xxxyy.add(tombol, java.awt.BorderLayout.LINE_END);

        jPanel5.setOpaque(false);
        xxxyy.add(jPanel5, java.awt.BorderLayout.LINE_START);

        getContentPane().add(xxxyy, java.awt.BorderLayout.CENTER);

        setBounds(0, 0, 1326, 624);
    }// </editor-fold>//GEN-END:initComponents

    private void textNISCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_textNISCaretUpdate
        int p = textNIS.getText().length();
        if (p >= 20) {

            cekNIS();
        } else {
            b_masuk.setVisible(false);
//            b_keluar.setVisible(false);
        }
    }//GEN-LAST:event_textNISCaretUpdate

    private void textNoHPCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_textNoHPCaretUpdate
        // TODO add your handling code here:
    }//GEN-LAST:event_textNoHPCaretUpdate

    private void textNamaCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_textNamaCaretUpdate
        // TODO add your handling code here:
    }//GEN-LAST:event_textNamaCaretUpdate

    private void textPanggilanCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_textPanggilanCaretUpdate
        // TODO add your handling code here:
    }//GEN-LAST:event_textPanggilanCaretUpdate

    private void b_masukActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_masukActionPerformed
//        simpanMasuk();
    }//GEN-LAST:event_b_masukActionPerformed

    private void scanNISCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_scanNISCaretUpdate
        String ss = scanNIS.getText().trim();
        if (ss.length() == 0) {

        } else if (ss.length() <= 20) {
            textNIS.setText(scanNIS.getText().trim());
        }
    }//GEN-LAST:event_scanNISCaretUpdate

    private void btnPembayarnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPembayarnActionPerformed
        simpanAntrian("PEMBAYARAN");
    }//GEN-LAST:event_btnPembayarnActionPerformed

    private void btnKonsultasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnKonsultasiActionPerformed
        simpanAntrian("KONSULTASI");
    }//GEN-LAST:event_btnKonsultasiActionPerformed

    private void btnTrialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTrialActionPerformed
        simpanAntrian("TRIAL");
    }//GEN-LAST:event_btnTrialActionPerformed

    private void btnPendaftaranActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPendaftaranActionPerformed
        simpanAntrian("PENDAFTARAN");
    }//GEN-LAST:event_btnPendaftaranActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(jendelaUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(jendelaUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(jendelaUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(jendelaUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new jendelaUtamaWithAntrian().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton b_masuk;
    private javax.swing.JButton btnKonsultasi;
    private javax.swing.JButton btnPembayarn;
    private javax.swing.JButton btnPendaftaran;
    private javax.swing.JButton btnTrial;
    private javax.swing.JPanel data;
    private javax.swing.JLabel gambar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JLabel jam;
    private javax.swing.JTextField scanNIS;
    private javax.swing.JTextField textNIS;
    private javax.swing.JTextField textNama;
    private javax.swing.JTextField textNoHP;
    private javax.swing.JTextField textPanggilan;
    private javax.swing.JPanel tombol;
    private javax.swing.JPanel xxxyy;
    // End of variables declaration//GEN-END:variables
}
