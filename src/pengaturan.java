import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/*
 * pengaturan.java
 *
 * Created on 19 September 2010, 21:39
 */

/**
 *
 * @author  Iweks
 */
public class pengaturan extends javax.swing.JFrame {
    Connection koneksiLocal;
   
    /**
     * Creates new form pengaturan
     */
    public pengaturan() {
        initComponents();
        String folder = System.getProperty("user.dir");
        String driverJDBC = "jdbc:odbc:Driver={Microsoft Access Driver (*.mdb)};DBQ=" + folder + "\\" + "Setting" + "\\" + "Setting.mdb;PWD=javakuternate24";
        koneksiLocal = getConnection("sun.jdbc.odbc.JdbcOdbcDriver", driverJDBC, "", "javakuternate24");
        if(koneksiLocal != null){
            cekDatabase();
        }
        setVisible(true);
    }
    
   
    private boolean cekDatabase(){
        t_cek.setText("");
        b_db.setEnabled(false);
        String sql = "SELECT * FROM db";
        boolean ok = false;
        try {
            java.sql.PreparedStatement stat = koneksiLocal.prepareStatement(sql);
            java.sql.ResultSet rset = stat.executeQuery();
            if (rset.next()) {
                String jdbc = rset.getString("driver");
                String url = rset.getString("url");
                String user = rset.getString("user");
                String pass = rset.getString("pass");
                String ip = rset.getString("ip");
                String port = rset.getString("port");
                String nama = rset.getString("nama");
                
                t_user.setText(user);
                t_pass.setText(pass);
                t_ip.setText(ip);
                t_port.setText(port);
                t_nama.setText(nama);
                ok = true;
                
            }
        } catch (SQLException ex) {
            ok =false;
            // ex.printStackTrace();
            System.out.println("ERROR DATABASE "+ ex.getMessage());
        }
        
        return ok;
    }
  
    private boolean cekKoneksi(){
        boolean ok = false;
        
        String jdbc = t_driver.getSelectedItem().toString();
        //  String url = t_url.getText();
        String user = t_user.getText();
        String pass = t_pass.getText();
        String ip = t_ip.getText();
        String port = t_port.getText();
        String nama = t_nama.getText();
         String url = "jdbc:postgresql://"+ ip + ":" + port + "/" + nama;
        if(jdbc.equalsIgnoreCase("com.microsoft.sqlserver.jdbc.SQLServerDriver")){
            //  "jdbc:sqlserver://192.168.10.2:1433;DatabaseName=ARISTA_VERSI_3.1";
             url = "jdbc:sqlserver://"+ ip + ":" + port + ";DatabaseName=" + nama;
        } else if(jdbc.equalsIgnoreCase("com.mysql.jdbc.Driver")){
            //  "jdbc:sqlserver://192.168.10.2:1433;DatabaseName=ARISTA_VERSI_3.1";
             url = "jdbc:mysql://"+ ip + ":" + port + "/" + nama;
        }
       
        t_url.setText(url);
        
        Connection kk = getConnection(jdbc, url, user, pass);
        if(kk == null){
            ok = false;
        } else {
            ok = true;
        }
        return ok;
    }
    private boolean simpanDB(){
        boolean ok = false;
        String jdbc = t_driver.getSelectedItem().toString();
        String url = t_url.getText();
        String user = t_user.getText();
        String pass = t_pass.getText();
        String ip = t_ip.getText();
        String port = t_port.getText();
        String nama = t_nama.getText();
        
        String sql_1 = "DELETE FROM db";
        String sql_2 = "INSERT INTO db(driver,url,user,pass,ip,port,nama) VALUES(?,?,?,?,?,?,?)";
        try {
            
            PreparedStatement stat = koneksiLocal.prepareStatement(sql_1);
            stat.executeUpdate();
            
            PreparedStatement stat2 = koneksiLocal.prepareStatement(sql_2);
            stat2.setString(1, jdbc);
            stat2.setString(2, url);
            stat2.setString(3, user);
            stat2.setString(4, pass);
            stat2.setString(5, ip);
            stat2.setString(6, port);
            stat2.setString(7, nama);
            stat2.executeUpdate();
            
            ok = true;
        } catch (SQLException ex) {
            ok = false;
            ex.printStackTrace();
        }
        return ok;
    }
  
    public Connection getConnection(String driverJDBC, String url, String user, String password){
        boolean driver = false;
        Connection koneksi = null;
        try {
            // Penyambungan Driver
            Class.forName(driverJDBC);
            driver = true;
            // Apabila Kelas Driver Tidak Ditemukan
        } catch (ClassNotFoundException not) {
            koneksi = null;
            //  JOptionPane.showMessageDialog(null, "Fail Loading Driver : "
            //          +driverJDBC+"\nDetail : "+not.getMessage(),"Error Database", JOptionPane.OK_OPTION);
            System.out.println(not.getMessage());
            //  System.exit(0);
        }
        
        if (driver == true) {
            
            try {
                koneksi = java.sql.DriverManager.getConnection(url, user, password);
                
            } // Akhir try
            catch (java.sql.SQLException sqln) {
                koneksi = null;
                System.out.println("Fail connecting with Database Server..."
                        +"\nDetail : "+sqln.getMessage());
                //     JOptionPane.showMessageDialog(null, "Fail connecting with Database Server..."
                //            +"\nDetail : "+sqln.getMessage(),"Error Database", JOptionPane.OK_OPTION);
                //    System.exit(0);
                
            }
        }
        
        return koneksi;
    }
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        data = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        t_driver = new javax.swing.JComboBox();
        t_ip = new javax.swing.JTextField();
        t_port = new javax.swing.JTextField();
        t_nama = new javax.swing.JTextField();
        t_user = new javax.swing.JTextField();
        b_test = new javax.swing.JButton();
        t_cek = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        t_url = new javax.swing.JTextField();
        t_pass = new javax.swing.JPasswordField();
        jPanel6 = new javax.swing.JPanel();
        b_db = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        b_db1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Pengaturan Database");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().add(jPanel2, java.awt.BorderLayout.NORTH);
        getContentPane().add(jPanel3, java.awt.BorderLayout.SOUTH);
        getContentPane().add(jPanel4, java.awt.BorderLayout.EAST);
        getContentPane().add(jPanel5, java.awt.BorderLayout.WEST);

        data.setBackground(new java.awt.Color(255, 255, 255));
        data.setLayout(new java.awt.BorderLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("Driver JDBC");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 90, 20));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("IP Server");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 90, 20));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Port");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 90, 20));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Nama DB");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, 90, 20));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("User");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 130, 90, 20));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setText("Password");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 160, 90, 20));

        t_driver.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "com.mysql.jdbc.Driver", "org.postgresql.Driver", "com.microsoft.sqlserver.jdbc.SQLServerDriver" }));
        jPanel1.add(t_driver, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 10, 150, -1));
        jPanel1.add(t_ip, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 40, 150, -1));
        jPanel1.add(t_port, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, 50, -1));
        jPanel1.add(t_nama, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 100, 150, -1));
        jPanel1.add(t_user, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 130, 150, -1));

        b_test.setText("Test Koneksi");
        b_test.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_testActionPerformed(evt);
            }
        });
        jPanel1.add(b_test, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 190, -1, -1));

        t_cek.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        t_cek.setText("jLabel7");
        jPanel1.add(t_cek, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 190, 180, 20));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel8.setText(":");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 10, 20, 20));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel9.setText(":");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 40, 20, 20));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel10.setText(":");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 70, 20, 20));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel11.setText(":");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 100, 20, 20));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel12.setText(":");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 130, 20, 20));

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel13.setText(":");
        jPanel1.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 160, 20, 20));
        jPanel1.add(t_url, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 10, 0, 0));
        jPanel1.add(t_pass, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 160, 150, 20));

        data.add(jPanel1, java.awt.BorderLayout.CENTER);

        jPanel6.setBackground(new java.awt.Color(204, 204, 204));
        jPanel6.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        b_db.setText("Simpan Pengaturan");
        b_db.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_dbActionPerformed(evt);
            }
        });
        jPanel6.add(b_db);

        data.add(jPanel6, java.awt.BorderLayout.SOUTH);

        jTabbedPane1.addTab("Database", data);

        jPanel7.setLayout(new java.awt.BorderLayout());

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.add(jPanel8, java.awt.BorderLayout.CENTER);

        jPanel9.setBackground(new java.awt.Color(204, 204, 204));
        jPanel9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        b_db1.setText("Simpan Pengaturan");
        b_db1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_db1ActionPerformed(evt);
            }
        });
        jPanel9.add(b_db1);

        jPanel7.add(jPanel9, java.awt.BorderLayout.SOUTH);

        jTabbedPane1.addTab("Email Account", jPanel7);

        getContentPane().add(jTabbedPane1, java.awt.BorderLayout.CENTER);

        setSize(new java.awt.Dimension(437, 452));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing

    }//GEN-LAST:event_formWindowClosing
                    
    private void b_dbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_dbActionPerformed
        if(simpanDB()){
            cekDatabase();
            t_cek.setText("Sukses Menyimpan");
            t_cek.setForeground(Color.BLUE);
            JOptionPane.showMessageDialog(this,"Sukses Menyimpan Pengaturan Database");
            this.dispose();
            new jendelaUtamaWithAntrian();
        } else {
            JOptionPane.showMessageDialog(this,"GAGAL MENYIMPAN KONFIGURASI DATABASE");
            t_cek.setText("GAGAL Menyimpan");
            t_cek.setForeground(Color.RED);
            b_db.setEnabled(false);
        }
    }//GEN-LAST:event_b_dbActionPerformed
    
    private void b_testActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_testActionPerformed
        if(cekKoneksi()){
            t_cek.setText("Koneksi Sukses");
            t_cek.setForeground(Color.BLUE);
            b_db.setEnabled(true);
        } else {
            t_cek.setText("Koneksi Gagal");
            t_cek.setForeground(Color.RED);
            b_db.setEnabled(false);
        }
    }//GEN-LAST:event_b_testActionPerformed

    private void b_db1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_db1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_b_db1ActionPerformed
    
    /**
     * @param args the command line arguments
     */
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton b_db;
    private javax.swing.JButton b_db1;
    private javax.swing.JButton b_test;
    private javax.swing.JPanel data;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel t_cek;
    private javax.swing.JComboBox t_driver;
    private javax.swing.JTextField t_ip;
    private javax.swing.JTextField t_nama;
    private javax.swing.JPasswordField t_pass;
    private javax.swing.JTextField t_port;
    private javax.swing.JTextField t_url;
    private javax.swing.JTextField t_user;
    // End of variables declaration//GEN-END:variables
    
    public class MyComboBoxEditor extends DefaultCellEditor {
        public MyComboBoxEditor(String[] items) {
            super(new JComboBox(items));
        }
    }
}
