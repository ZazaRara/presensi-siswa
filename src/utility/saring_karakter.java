package utility;
import javax.swing.*;
import javax.swing.text.*;

public class saring_karakter {
    public saring_karakter() {
    }
    
    byte length;
    public saring_karakter(byte length) {
        this.length = length;
    }
    
    /** methode ini dipakai untuk mengeset dan mendapatkan filter
     *  @return mengembalikan filter bertype PlainDocument
     *  Menyaring Hanya karakter angka (Digit) yang bisa dimasukkan
     */
    public PlainDocument setOnlyDigit() {
        PlainDocument filterDigit = new PlainDocument() {
            public void insertString(int offs, String str, AttributeSet a) throws
                    BadLocationException {
                StringBuffer buffer = new StringBuffer();
                int s = 0;
                char[] dataInput = str.toCharArray();
                // Memeriksa semua data yang dimasukkan
                for (int i = 0; i < dataInput.length; i++) {
                    // Menyaring Apakah data masukkan berupa DIGIT ??
                    boolean isOnlyDigit = Character.isLetter(dataInput[i]);
                    if (isOnlyDigit == false) {
                        dataInput[s] = dataInput[i];
                        s++;
                    }
                }
                buffer.append(dataInput, 0, s);
                super.insertString(offs, new String(buffer), a);
            }
        };
        return filterDigit;
    }
    
    public PlainDocument setAllOnlyDigit() {
        PlainDocument filterDigit = new PlainDocument() {
            public void insertString(int offs, String str, AttributeSet a) throws
                    BadLocationException {
                StringBuffer buffer = new StringBuffer();
                int s = 0;
                char[] dataInput = str.toCharArray();
                // Memeriksa semua data yang dimasukkan
                for (int i = 0; i < dataInput.length; i++) {
                    // Menyaring Apakah data masukkan berupa DIGIT ??
                    boolean isOnlyDigit = Character.isDigit(dataInput[i]);
                    if (dataInput[i] == ',') {
                        dataInput[s] = dataInput[i];
                        s++;
                    }
                    if (isOnlyDigit == true) {
                        dataInput[s] = dataInput[i];
                        s++;
                    }
                }
                buffer.append(dataInput, 0, s);
                super.insertString(offs, new String(buffer), a);
            }
        };
        return filterDigit;
    }
    
    /** methode ini dipakai untuk mengeset dan mendapatkan filter
     *  @return mengembalikan filter bertype PlainDocument
     *  Menyaring Hanya karakter huruf (Letter) yang bisa dimasukkan
     */
    public PlainDocument setOnlyLetter() {
        PlainDocument filterLetter = new PlainDocument() {
            public void insertString(int offs, String str, AttributeSet a) throws
                    BadLocationException {
                StringBuffer buffer = new StringBuffer();
                int s = 0;
                char[] dataInput = str.toCharArray();
                // Memeriksa semua data yang dimasukkan
                for (int i = 0; i < dataInput.length; i++) {
                    // Menyaring Apakah data masukkan berupa LETTER ??
                    boolean isOnlyLetter = Character.isDigit(dataInput[i]);
                    
                    if (isOnlyLetter == false) {
                        dataInput[s] = dataInput[i];
                        s++;
                    }
                }
                buffer.append(dataInput, 0, s);
                super.insertString(offs, new String(buffer).toUpperCase(), a);
            }
        };
        return filterLetter;
    }
    
    /** methode ini dipakai untuk mengeset dan mendapatkan filter
     *  @return mengembalikan filter bertype PlainDocument
     *  Menyaring Karakter menjadi hurif besar (Upper Case)
     */
    public PlainDocument setToUpperCase() {
        PlainDocument filterUpper = new PlainDocument() {
            public void insertString(int offs, String str, AttributeSet a) throws
                    BadLocationException {
                char[] upper = str.toCharArray();
                for (int i = 0; i < upper.length; i++) {
                    // Menjadi upper case
                    upper[i] = Character.toUpperCase(upper[i]);
                }
                super.insertString(offs, new String(upper), a);
            }
        };
        return filterUpper;
    }
    
    JTextField texts;
    public PlainDocument getFilter(JTextField text) {
        texts = text;
        PlainDocument filter = new javax.swing.text.PlainDocument() {
            public void insertString(int offs, String str, AttributeSet a) throws
                    BadLocationException {
                int ab = texts.getText().length();
                if (ab < length) {
                    super.insertString(offs, str, a);
                }
            }
        };
        return filter;
    }
    
}
